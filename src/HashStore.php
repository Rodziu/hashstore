<?php
/**
 *  __    __ _     _     _       _       _     ___  __
 * / / /\ \ (_) __| |___(_) __ _| |_ __ (_)   / _ \/ /
 * \ \/  \/ / |/ _` |_  / |/ _` | | '_ \| |  / /_)/ /
 *  \  /\  /| | (_| |/ /| | (_| | | | | | |_/ ___/ /___
 *   \/  \/ |_|\__,_/___|_|\__,_|_|_| |_|_(_)/   \____/
 *
 */
namespace WidzialniPL;
/**
 * This trait allows one to store data in given path under md5 keys with optimal disk utilization
 *
 * @author Rodziu <mateusz.rohde@gmail.com>
 * @version 1.1
 */
trait HashStore {
	/**
	 * where would you like to store data?
	 * @var string
	 */
	public $hashStorePath = null;
	/**
	 * how much levels should our store have?
	 * @var int
	 */
	private $hashStoreDepth = 3;
	/**
	 * How much letters each folder should have (this gives 16^$hashStoreDirLength folders on each level)
	 * @var int
	 */
	private $hashStoreDirLength = 2;

	/**
	 * store data under given hash
	 *
	 * @param string $hash
	 * @param string $data
	 *
	 * @return string path to written file
	 *
	 * @throws \ErrorException
	 */
	public function hashStoreSet($hash, $data){
		$path = $this->hashStoreGetPath($hash, true);
		$umask = umask(0);
		file_put_contents("$this->hashStorePath/$path", $data);
		umask($umask);
		return $path;
	}

	/**
	 * read data or return false if it doesn't exist
	 *
	 * @param string $hash
	 *
	 * @return bool|string
	 * @throws \ErrorException
	 */
	public function hashStoreGet($hash){
		$path = $this->hashStoreGetPath($hash);
		if(file_exists("$this->hashStorePath/$path"))
			return file_get_contents("$this->hashStorePath/$path");
		return false;
	}

	/**
	 * remove data stored under given hash
	 *
	 * @param string $hash
	 *
	 * @return bool
	 * @throws \ErrorException
	 */
	public function hashStoreRemove($hash){
		if(file_exists($this->hashStorePath."/".$this->hashStoreGetPath($hash))){
			return @unlink($this->hashStorePath."/".$this->hashStoreGetPath($hash));
		}else{
			return 0;
		}
	}

	/**
	 * Get full path to requested data
	 *
	 * @param string $hash
	 * @param boolean $create
	 *
	 * @return string
	 * @throws \ErrorException
	 */
	public function hashStoreGetPath($hash, $create = false){
		if(is_null($this->hashStorePath)){
			throw new \ErrorException('Please set hashStorePath!');
		}
		$depth = 0;
		$path = "";
		for($i = 0; $i < 32; $i += $this->hashStoreDirLength){
			if($depth == $this->hashStoreDepth){
				break;
			}
			$path .= (!empty($path) ? '/' : '').$hash[$i].$hash[$i+1];
			$depth++;
		}
		if($create && !is_dir("$this->hashStorePath/$path")){
			$umask = umask(0);
			mkdir("$this->hashStorePath/$path", 0777, true);
			umask($umask);
		}
		return "$path/$hash";
	}
}