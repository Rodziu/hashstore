<?php
define('BASEDIR', dirname(__DIR__));
class HashStoreTest extends \PHPUnit_Framework_TestCase{
	use WidzialniPL\HashStore;

	public function setUp($noSQLTransaction = false){
		parent::setUp($noSQLTransaction);
	}

	/**
	 *
	 */
	public function testSetup(){
		if(is_dir(BASEDIR.'/data/hashStoreTest')){
			foreach(new RecursiveIteratorIterator(new RecursiveDirectoryIterator(BASEDIR.'/data/hashStoreTest', RecursiveDirectoryIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST) as $i){
				if($i->isDir()){
					rmdir($i->getPathname());
				}else{
					unlink($i->getPathname());
				}
			}
			rmdir(BASEDIR.'/data/hashStoreTest');
		}
	}

	/**
	 * @throws ErrorException
	 */
	public function testException(){
		$this->setExpectedException('ErrorException', 'Please set hashStorePath!');
		$this->hashStoreGetPath('test');

	}

	/**
	 * @throws ErrorException
	 */
	public function testGetPath(){
		$this->hashStorePath = BASEDIR.'/data/hashStoreTest';
		$this->assertEquals('97/43/a6/9743a66f914cc249efca164485a19c5c', $this->hashStoreGetPath('9743a66f914cc249efca164485a19c5c'));
	}

	/**
	 * @depends testGetPath
	 */
	public function testSet(){
		$this->hashStorePath = BASEDIR.'/data/hashStoreTest';
		$this->hashStoreSet('9743a66f914cc249efca164485a19c5c', '1');
		$this->assertFileExists($this->hashStorePath.'/97/43/a6/9743a66f914cc249efca164485a19c5c');
		$this->assertTrue(is_writable($this->hashStorePath.'/97/43/a6/9743a66f914cc249efca164485a19c5c'));
	}

	/**
	 * @depends testGetPath
	 * @depends testSet
	 */
	public function testGet(){
		$this->hashStorePath = BASEDIR.'/data/hashStoreTest';
		$this->assertEquals('1', $this->hashStoreGet('9743a66f914cc249efca164485a19c5c'));
	}

	/**
	 * @depends testGetPath
	 * @depends testSet
	 */
	public function testRemove(){
		$this->hashStorePath = BASEDIR.'/data/hashStoreTest';
		$this->assertTrue($this->hashStoreRemove('9743a66f914cc249efca164485a19c5c'), "Removing existing file");
		$this->assertFileNotExists($this->hashStorePath.'/97/43/a6/9743a66f914cc249efca164485a19c5c');
		$this->assertEquals(0, $this->hashStoreRemove('9743a66f914cc249efca164485a19c5d'), "Removing inexisting file");
	}
}
